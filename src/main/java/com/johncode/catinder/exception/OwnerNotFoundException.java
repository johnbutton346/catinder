package com.johncode.catinder.exception;

public class OwnerNotFoundException extends Exception {
    public OwnerNotFoundException(String message) {
        super(message);
    }
}
