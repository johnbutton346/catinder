package com.johncode.catinder.exception;

public class CatNotFoundException extends Exception{
    public CatNotFoundException(String message) {
        super(message);
    }
}
