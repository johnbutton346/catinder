package com.johncode.catinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatinderApplication.class, args);
	}

}
