package com.johncode.catinder.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CatDto extends BaseDto {

    private String catName;
    private Double weight;
}
