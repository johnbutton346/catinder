package com.johncode.catinder.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ResponseDto {

    private int httpCode;
    private CodeMessage codeMessage;
    private Page<? extends BaseDto> content;
}
