package com.johncode.catinder.dto;

public enum CodeMessage {
    OK,
    CREATED,
    NO_CONTENT,
    BAD_REQUEST,
    NOT_FOUND,
    ENTITY_NOT_FOUND
}
