package com.johncode.catinder.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OwnerDto extends BaseDto {

    private String ownerName;
    private String ownerSurname;
    private Integer age;
    private List<CatDto> cats;
}
