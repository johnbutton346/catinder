package com.johncode.catinder.contoller;

import com.johncode.catinder.dto.CodeMessage;
import com.johncode.catinder.dto.ResponseDto;
import com.johncode.catinder.entity.Cat;
import com.johncode.catinder.exception.CatNotFoundException;
import com.johncode.catinder.repository.CatRepository;
import com.johncode.catinder.service.CatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Collections;
import java.util.UUID;

// TODO: 5/20/2021 add logs + swagger

@Slf4j
@RestController
@RequestMapping("/cats")
public class CatController {

    private final CatService catService;

    public CatController(CatService catService) {
        this.catService = catService;
    }

    @PostMapping
    public ResponseEntity<ResponseDto> save(@RequestBody Cat cat) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setContent(catService.save(cat));
            log.info(String.format("Entity [%s] with id %s added to DB", cat.toString(), cat.getId()));
            return ResponseEntity.ok().body(responseDto);
        } catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    /*@GetMapping
    public ResponseEntity<ResponseDto> getAll() {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setContent(catService.findAll());
            log.info(String.format("An array of entities with the size of %s received", responseDto.getContent().size()));
            return ResponseEntity.ok(responseDto);
        } catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());;
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }*/

    @GetMapping("/page/{number}")
    public ResponseEntity getPage(@PathVariable Integer number) {
        return ResponseEntity.ok().body(catService.findAll(PageRequest.of(number, 20)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto> getOne(@PathVariable UUID id) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setContent(catService.getById(id));
            log.info(String.format("A single entity with id [%s] received", id));
            return ResponseEntity.ok(responseDto);
        } catch (CatNotFoundException e) {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setCodeMessage(CodeMessage.ENTITY_NOT_FOUND);
            return ResponseEntity.badRequest().body(responseDto);
        }
        catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto> delete(@PathVariable UUID id) {

        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            catService.delete(id);
            log.info(String.format("Entity with id [%s] deleted", id));
            return ResponseEntity.ok(responseDto);
        } catch (CatNotFoundException e) {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setCodeMessage(CodeMessage.ENTITY_NOT_FOUND);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
        catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setCodeMessage(CodeMessage.BAD_REQUEST);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseDto> update(@PathVariable UUID id, @RequestBody Cat cat) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setContent(catService.update(id, cat));
            log.info(String.format("A single entity with id [%s] updated, new data is [%s]", id, responseDto.getContent().stream().findFirst()));
            return ResponseEntity.ok().body(responseDto);
        } catch (CatNotFoundException e) {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setCodeMessage(CodeMessage.ENTITY_NOT_FOUND);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
        catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setCodeMessage(CodeMessage.BAD_REQUEST);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }
}
