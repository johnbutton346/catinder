package com.johncode.catinder.contoller;

import com.johncode.catinder.dto.CodeMessage;
import com.johncode.catinder.dto.OwnerDto;
import com.johncode.catinder.dto.ResponseDto;
import com.johncode.catinder.entity.Cat;
import com.johncode.catinder.entity.Owner;
import com.johncode.catinder.exception.CatNotFoundException;
import com.johncode.catinder.exception.OwnerNotFoundException;
import com.johncode.catinder.repository.OwnerRepository;
import com.johncode.catinder.service.OwnerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/owners")
public class OwnerController {

    private final OwnerService ownerService;

    public OwnerController(OwnerService ownerService) {
        this.ownerService = ownerService;
    }

    @GetMapping("/page/{number}")
    public ResponseEntity<ResponseDto> getAll(@PathVariable Integer number) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setContent(ownerService.findAll(PageRequest.of(number, 20)));
            log.info(String.format("An array of entities with the size of %s received", responseDto.getContent().getTotalElements()));
            return ResponseEntity.ok().body(responseDto);
        } catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setCodeMessage(CodeMessage.BAD_REQUEST);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto> getOne(@PathVariable UUID id) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setContent(ownerService.getById(id));
            log.info(String.format("A single entity with id [%s] received", id));
            return ResponseEntity.ok().body(responseDto);
        } catch (OwnerNotFoundException e) {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setCodeMessage(CodeMessage.ENTITY_NOT_FOUND);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        } catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setCodeMessage(CodeMessage.BAD_REQUEST);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @PostMapping
    public ResponseEntity<ResponseDto> save(@RequestBody Owner owner) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setContent(ownerService.save(owner));
            log.info(String.format("Entity [%s] with id %s added to DB", owner.toString(), owner.getId()));
            return ResponseEntity.ok().body(responseDto);
        } catch (Error e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setCodeMessage(CodeMessage.BAD_REQUEST);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto> delete(@PathVariable UUID id) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            ownerService.delete(id);
            log.info(String.format("Entity with id [%s] deleted", id));
            return ResponseEntity.ok().body(responseDto);
        } catch (OwnerNotFoundException e) {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setCodeMessage(CodeMessage.ENTITY_NOT_FOUND);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        } catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setCodeMessage(CodeMessage.BAD_REQUEST);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseDto> update(@PathVariable UUID id, @RequestBody Owner owner) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setContent(ownerService.update(id, owner));
            log.info(String.format("A single entity with id [%s] updated, new data is [%s]", id, responseDto.getContent().stream().findFirst()));
            return ResponseEntity.ok().body(responseDto);
        } catch (OwnerNotFoundException e) {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setCodeMessage(CodeMessage.ENTITY_NOT_FOUND);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        } catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setCodeMessage(CodeMessage.BAD_REQUEST);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @PutMapping("/get/{id}")
    public ResponseEntity<ResponseDto> getCat(@PathVariable UUID id, @RequestParam UUID catId) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setContent(ownerService.getCat(id, catId));
            log.info(String.format("Added a cat with id [%s] to owner with id [%s]", catId, id));
            return ResponseEntity.ok().body(responseDto);
        }  catch (OwnerNotFoundException e) {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setCodeMessage(CodeMessage.ENTITY_NOT_FOUND);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        } catch (CatNotFoundException e) {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setCodeMessage(CodeMessage.ENTITY_NOT_FOUND);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        } catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setCodeMessage(CodeMessage.BAD_REQUEST);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @PutMapping("/get")
    public ResponseEntity<ResponseDto> getCat(@RequestParam UUID id, @RequestBody Cat cat) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setContent(ownerService.getCat(id, cat));
            log.info(String.format("Created new cat and added to owner with id [%s]", id));
            return ResponseEntity.ok().body(responseDto);
        }  catch (OwnerNotFoundException e) {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setCodeMessage(CodeMessage.ENTITY_NOT_FOUND);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }  catch (CatNotFoundException e) {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setCodeMessage(CodeMessage.ENTITY_NOT_FOUND);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        } catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setCodeMessage(CodeMessage.BAD_REQUEST);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @GetMapping("/{id}/cats")
    public ResponseEntity<ResponseDto> getOwnerCats(@PathVariable UUID id) {
        ResponseDto responseDto = new ResponseDto();
        try {
            OwnerDto owner = ownerService.getById(id).getContent().get(0);
            if (owner.getCats().isEmpty()) {
                responseDto.setCodeMessage(CodeMessage.NO_CONTENT);
            }
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setContent(new PageImpl<>(owner.getCats()));
            log.info(String.format("Received array of cats by owner with id [%s] and size %d", id, responseDto.getContent().getTotalElements()));
            return ResponseEntity.ok().body(responseDto);
        } catch (OwnerNotFoundException e) {
            responseDto.setHttpCode(HttpStatus.OK.value());
            responseDto.setCodeMessage(CodeMessage.ENTITY_NOT_FOUND);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        } catch (Exception e) {
            responseDto.setHttpCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setCodeMessage(CodeMessage.BAD_REQUEST);
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(responseDto);
        }
    }
}
