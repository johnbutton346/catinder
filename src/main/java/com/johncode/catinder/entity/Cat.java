package com.johncode.catinder.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "cats", schema = "cats")
public class Cat extends BaseEntity {

    // TODO: 5/21/2021 владелец + медали

    // TODO: 5/21/2021 add enum field to DB like gender, color, etc 

    private String catName;
    private Double weight;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id")
    private Owner owner;

}
