package com.johncode.catinder.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(schema = "cats", name = "owners")
public class Owner extends BaseEntity{

    private String ownerName;
    private String ownerSurname;
    private Integer age;

    @OneToMany(mappedBy = "owner", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.LAZY)
    private List<Cat> cats = new ArrayList<>();
}
