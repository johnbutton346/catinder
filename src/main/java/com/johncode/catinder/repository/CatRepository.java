package com.johncode.catinder.repository;

import com.johncode.catinder.entity.Cat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.Nullable;

import java.util.UUID;

public interface CatRepository extends JpaRepository<Cat, UUID> {

    Page<Cat> findAll(@Nullable Pageable pageable);
}
