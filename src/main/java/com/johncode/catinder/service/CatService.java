package com.johncode.catinder.service;

import com.johncode.catinder.configuration.SpringConfig;
import com.johncode.catinder.dto.CatDto;
import com.johncode.catinder.entity.Cat;
import com.johncode.catinder.exception.CatNotFoundException;
import com.johncode.catinder.repository.CatRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CatService {

    private final CatRepository catRepository;
    private final SpringConfig springConfig;

    public CatService(CatRepository catRepository, SpringConfig springConfig) {
        this.catRepository = catRepository;
        this.springConfig = springConfig;
    }

    public List<CatDto> findAll() {
        return catRepository.findAll().stream().map(cat -> springConfig.modelMapper().map(cat, CatDto.class)).collect(Collectors.toList());
    }

    public Page<CatDto> findAll(Pageable pageable) {
        List<CatDto>  cats  = catRepository.findAll(pageable).stream().map(cat -> springConfig.modelMapper().map(cat, CatDto.class)).collect(Collectors.toList());
        return new PageImpl<>(cats, pageable, cats.size());
    }

    public Page<CatDto> getById(UUID id) throws CatNotFoundException {
        Cat cat = catRepository.findById(id).orElse(null);
        if (cat == null)
            throw new CatNotFoundException("Can't found cat with id: " + id);
        return new PageImpl<>(Collections.singletonList(springConfig.modelMapper().map(cat, CatDto.class)));
    }

    public Page<CatDto> save(Cat cat) {
        return new PageImpl<>(Collections.singletonList(springConfig.modelMapper().map(catRepository.save(cat), CatDto.class)));
    }

    public void delete(UUID id) throws CatNotFoundException {
        Cat cat = catRepository.findById(id).orElse(null);

        if (cat == null)
            throw new CatNotFoundException("Can't found cat with id: " + id);
        catRepository.delete(cat);

    }

    public Page<CatDto> update(UUID id, Cat catEntity) throws CatNotFoundException {
        Cat cat = catRepository.findById(id).orElse(null);
        if (cat == null) {
            throw new CatNotFoundException("Can't found cat with id: " + id);
        }
        if (catEntity.getCatName() != null) {
            cat.setCatName(catEntity.getCatName());
        }
        if (catEntity.getWeight() != null) {
            cat.setWeight(catEntity.getWeight());
        }
        return new PageImpl<>(Collections.singletonList(springConfig.modelMapper().map(catRepository.save(cat), CatDto.class)));
    }
}
