package com.johncode.catinder.service;

import com.johncode.catinder.configuration.SpringConfig;
import com.johncode.catinder.dto.OwnerDto;
import com.johncode.catinder.entity.Cat;
import com.johncode.catinder.entity.Owner;
import com.johncode.catinder.exception.CatNotFoundException;
import com.johncode.catinder.exception.OwnerNotFoundException;
import com.johncode.catinder.repository.CatRepository;
import com.johncode.catinder.repository.OwnerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OwnerService {

    private final OwnerRepository ownerRepository;
    private final CatRepository catRepository;
    private final SpringConfig springConfig;

    public OwnerService(OwnerRepository ownerRepository, CatRepository catRepository, SpringConfig springConfig) {
        this.ownerRepository = ownerRepository;
        this.catRepository = catRepository;
        this.springConfig = springConfig;
    }

    public List<OwnerDto> findAll() {
        return ownerRepository.findAll().stream().map(owner -> springConfig.modelMapper().map(owner, OwnerDto.class)).collect(Collectors.toList());
    }

    public Page<OwnerDto> findAll(Pageable pageable) {
        List<OwnerDto> owners = ownerRepository.findAll().stream().map(owner -> springConfig.modelMapper().map(owner, OwnerDto.class)).collect(Collectors.toList());
        return  new PageImpl<>(owners, pageable, owners.size());
    }

    public Page<OwnerDto> getById(UUID id) throws OwnerNotFoundException {
        Owner owner = ownerRepository.findById(id).orElse(null);
        if (owner == null)
            throw new OwnerNotFoundException("Can't found owner with id: " + id);
        return new PageImpl<>(Collections.singletonList(springConfig.modelMapper().map(owner, OwnerDto.class)));
    }

    public Page<OwnerDto> save(Owner owner) {
        return new PageImpl<>(Collections.singletonList(springConfig.modelMapper().map(ownerRepository.save(owner), OwnerDto.class)));
    }

    public void delete(UUID id) throws OwnerNotFoundException {
        Owner owner = ownerRepository.findById(id).orElse(null);
        if (owner == null)
            throw new OwnerNotFoundException("Can't found owner with id: " + id);
        ownerRepository.delete(owner);

    }

    public Page<OwnerDto> update(UUID id, Owner ownerEntity) throws OwnerNotFoundException {
        Owner owner = ownerRepository.findById(id).orElse(null);
        if (owner == null)
            throw new OwnerNotFoundException("Can't found owner with id: " + id);
        if (ownerEntity.getOwnerName() != null)
            owner.setOwnerName(ownerEntity.getOwnerName());
        if (ownerEntity.getOwnerSurname() != null)
            owner.setOwnerSurname(ownerEntity.getOwnerSurname());
        if (ownerEntity.getAge() != null)
            owner.setAge(ownerEntity.getAge());
        return new PageImpl<>(Collections.singletonList(springConfig.modelMapper().map(ownerRepository.save(owner), OwnerDto.class)));
    }

    public Page<OwnerDto> getCat(UUID id, UUID catId) throws OwnerNotFoundException, CatNotFoundException {
        Owner owner = ownerRepository.findById(id).orElse(null);
        Cat cat = catRepository.findById(catId).orElse(null);
        if (owner == null) {
            throw new OwnerNotFoundException("Can't found owner with id: " + id);
        }
        if (cat == null) {
            throw new CatNotFoundException("Can't found cat with id: " + id);
        }
        cat.setOwner(owner);
        catRepository.save(cat);
        return new PageImpl<>(Collections.singletonList(springConfig.modelMapper().map(ownerRepository.save(owner), OwnerDto.class)));
    }

    @Transactional
    public Page<OwnerDto> getCat(UUID id, Cat cat) throws OwnerNotFoundException, CatNotFoundException {
        Owner owner = ownerRepository.findById(id).orElse(null);
        if (owner == null) {
            throw new OwnerNotFoundException("Can't found owner with id: " + id);
        }
        if (cat == null) {
            throw new CatNotFoundException("Can't found cat with id: " + id);
        }
        cat.setOwner(owner);
        cat = catRepository.save(cat);
        owner.getCats().add(cat);
        owner = ownerRepository.save(owner);
        return new PageImpl<>(Collections.singletonList(springConfig.modelMapper().map(owner, OwnerDto.class)));
    }
}
