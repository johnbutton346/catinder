CREATE TABLE IF NOT EXISTS cats.owners (
    id UUID not null,
    owner_name varchar(50),
    owner_surname varchar(50),
    age int,
    primary key(id)
);