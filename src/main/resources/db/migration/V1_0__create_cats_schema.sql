CREATE TABLE IF NOT EXISTS cats.cat_entity(
    id uuid NOT NULL,
    catName varchar(20),
    weight decimal,
  PRIMARY KEY (id)
);