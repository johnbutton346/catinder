alter table cats.cats
add column owner_id uuid;

alter table cats.cats
add constraint fk_owners_cats
foreign key (owner_id) references cats.owners(id);